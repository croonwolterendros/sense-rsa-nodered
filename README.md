This module contains three nodes for Node-RED which allow you to easily create a keypair and encrypt or decrypt messages.

rsa-encrypt: encrypts input payload (strings or objects) using an RSA public key. Outputs an encrypted Base64 string.
Alternatively, when a message topic has been set to 'public-key', this will set the public key of this node to the incoming msg.payload.

rsa-decrypt: decrypts input payload using an RSA private key. Outputs a string or JSON object.
Alternatively, when a message topic has been set to 'private-key', this will set the according private key of this node to msg.payload.

rsa-generate-keypair: Generates a new RSA keypair.
This node can be connected to rsa-encrypt and rsa-decrypt to set their keys to the newly generated values.