module.exports = function(RED) 
{
    function Encrypt(config) 
    {
        let crypto = require('crypto');

        RED.nodes.createNode(this,config);
        var node = this;

        node.on('input', function(msg) 
        {
			var replaced = config.privatekey.replace("-----BEGIN ENCRYPTED PRIVATE KEY-----", "")
			replaced = replaced.replace("-----END ENCRYPTED PRIVATE KEY-----", "")
			
			replaced = "-----BEGIN ENCRYPTED PRIVATE KEY-----\n" + replaced + "\n-----END ENCRYPTED PRIVATE KEY-----";
			
			if(this.context().privateKey === "")
				this.context().privateKey = replaced;
			
            let keyData = this.context().privateKey || replaced;

            if(msg.topic === "private-key")
            {
                this.context().privateKey = msg.payload;
            }
            else if(msg.topic === "keypair")
            {
                this.context().privateKey = msg.payload.privateKey;
            }
            else
            {
                let buffer = Buffer.from(msg.payload, 'base64')

                let decrypted = crypto.privateDecrypt({key: keyData, passphrase: ''}, buffer);

                msg.payload = decrypted.toString('utf8');
                
                let pl = msg.payload;
                try 
                {
                    pl = JSON.parse(pl);
                    msg.payload = pl;
                } 
                catch (e) 
                {
                    //no json I guess
                }
                
                node.send(msg);
            }
        });
    }

    RED.nodes.registerType("rsa-decrypt", Encrypt);
}